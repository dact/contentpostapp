import socket


class WebApp:
    def __init__(self, ip, port):
        mysocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mysocket.bind((ip, port))
        mysocket.listen(5)
        while True:
            try:
                print("waiting...")
                (recvSocket, address) = mysocket.accept()
                print(address)
                print("HTTP request received: ")
                received = recvSocket.recv(2048)
                print(received)
                petition = self.parse(received)
                http, html = self.process(petition)

                response = "HTTP/1.1 " + http + "\r\n\r\n" + html + "\r\n"

                recvSocket.send(response.encode('utf-8'))
                recvSocket.close()
            except KeyboardInterrupt:
                print("\nServer closed")
                exit(1)
                
    def parse(self, received):
        recibido = received.decode()
        return recibido.split(' ')[1]

    def process(self, analyzed):
        http = "200 OK"
        html = f"<html><body><h1>Hello World! Tu peticion es {analyzed}</h1>" \
               + "</body></html>"
        return http, html


if __name__ == "__main__":
    webapp = WebApp('localhost', 1250)

