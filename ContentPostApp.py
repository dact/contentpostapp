import webapp

form = '<form action="" method="post" class="form-example">' \
             + '<div class="form-example"><label for="nombre_recurso">Recurso: </label>' \
             + '<input type="text" name="nombre_recurso" id="nombre_recurso" required>' \
             + '</div>' \
             + '<div class="form-example"><label for="contenido">Contenido: </label>' \
             + '<input type="contenido" name="contenido" id="contenido" required>' \
             + '</div>' \
             + '<div class="form-example">' \
             + '<input type="submit" value="Enviar">' \
             + '</div></form>'


class ContentPost(webapp.WebApp):
    recursos = {'/': 'P&aacute;gina principal', '/alex': 'P&aacute;gina de Alex'}

    def parse(self, received):
        recibido = received.decode()
        metodo = recibido.split(' ')[0]
        recurso = recibido.split(' ')[1]
        if metodo == "POST":
            body = recibido.split('\r\n\r\n')[1]
        else:
            body = None

        return metodo, recurso, body
        
def process(self, analyzed):
        metodo, recurso, body = analyzed
        html = "<html><body>" + form

        if metodo == "POST":
            parts = ' '.join(' '.join(body.split('&')).split('=')).split()
            recurso = parts[1].replace("%2F", "/")
            contenido = parts[3].replace("+", " ")
            self.recursos[recurso] = contenido

        if recurso in self.recursos.keys():
            http = "200 OK"
            html += self.recursos[recurso] + "</body></html>"
        else:
            http = "404 Not Found"
            html += "<img src='https://user-images.githubusercontent.com/24848110/33519396-7e56363c-d79d-11e7-969b" \
                    "-09782f5ccbab.png'/>" + "</body></html>\r\n"
        return http, html


if __name__ == "__main__":
    contentpost = ContentPost('localhost', 1250)

